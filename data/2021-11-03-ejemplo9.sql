﻿DROP DATABASE IF EXISTS ejemplo9;
CREATE DATABASE ejemplo9;
USE ejemplo9;

CREATE TABLE camioneros(
  dni varchar(12),
  nombre varchar(100),
  direccion varchar(100),
  telefono varchar(15),
  poblacion varchar(100),
  salario float,
  PRIMARY KEY(dni)
);

CREATE TABLE camion(
  matricula char(7),
  modelo varchar(100),
  tipo varchar(100),
  potencia int,
  PRIMARY KEY (matricula)
);

CREATE TABLE paquetes(
  codigo int,
  descripcion varchar(100),
  direccion varchar(100),
  destinatario varchar(100),
  PRIMARY KEY(codigo)
);

CREATE TABLE provincias(
  codigo int,
  nombre varchar(100),
  PRIMARY KEY(codigo)
);

CREATE TABLE conduce(
  dni_camionero varchar(12),
  mat_camion char(7),
  PRIMARY KEY(dni_camionero,mat_camion)
);

CREATE TABLE distribuye(
  dni_camionero varchar(12),
  cod_paquete int,
  PRIMARY KEY (dni_camionero,cod_paquete),
  UNIQUE KEY (cod_paquete)
);

CREATE TABLE destinado(
  cod_provincia int,
  cod_paquete int,
  PRIMARY KEY (cod_provincia,cod_paquete),
  UNIQUE KEY(cod_paquete)
);

ALTER TABLE conduce
  ADD CONSTRAINT fk_conduce_camionero 
    FOREIGN KEY(dni_camionero) REFERENCES camioneros(dni) 
    ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT fk_conduce_camion 
    FOREIGN KEY(mat_camion) REFERENCES camion(matricula)
    ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE distribuye 
 ADD CONSTRAINT fk_distribuye_camionero 
    FOREIGN KEY (dni_camionero) REFERENCES camioneros(dni)
    ON DELETE CASCADE ON UPDATE CASCADE, 
  ADD CONSTRAINT fk_distribuye_paquete 
    FOREIGN KEY (cod_paquete) REFERENCES paquetes(codigo)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE destinado 
  ADD CONSTRAINT fk_destinado_provincia 
    FOREIGN KEY (cod_provincia) REFERENCES provincias(codigo)
      ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT fk_destinado_paquete 
    FOREIGN KEY (cod_paquete) REFERENCES paquetes(codigo)
      ON DELETE CASCADE ON UPDATE CASCADE;
