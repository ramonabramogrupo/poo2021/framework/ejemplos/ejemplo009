<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Destinado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="destinado-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_provincia')->textInput() ?>

    <?= $form->field($model, 'cod_paquete')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
