<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Destinado */

$this->title = 'Create Destinado';
$this->params['breadcrumbs'][] = ['label' => 'Destinados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="destinado-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
