<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Destinados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="destinado-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Destinado', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_provincia',
            'cod_paquete',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
