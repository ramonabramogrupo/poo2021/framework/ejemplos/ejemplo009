<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Destinado */

$this->title = $model->cod_provincia;
$this->params['breadcrumbs'][] = ['label' => 'Destinados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="destinado-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cod_provincia' => $model->cod_provincia, 'cod_paquete' => $model->cod_paquete], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cod_provincia' => $model->cod_provincia, 'cod_paquete' => $model->cod_paquete], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod_provincia',
            'cod_paquete',
        ],
    ]) ?>

</div>
