<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Destinado */

$this->title = 'Update Destinado: ' . $model->cod_provincia;
$this->params['breadcrumbs'][] = ['label' => 'Destinados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_provincia, 'url' => ['view', 'cod_provincia' => $model->cod_provincia, 'cod_paquete' => $model->cod_paquete]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="destinado-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
