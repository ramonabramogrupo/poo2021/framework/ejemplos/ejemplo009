<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Conduce */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conduce-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni_camionero')->dropDownList($listadoCamioneros) ?>

    <?= $form->field($model, 'mat_camion')->dropDownList($listadoCamiones) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
