<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conduce */

$this->title = 'Update Conduce: ' . $model->dni_camionero;
$this->params['breadcrumbs'][] = ['label' => 'Conduces', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni_camionero, 'url' => ['view', 'dni_camionero' => $model->dni_camionero, 'mat_camion' => $model->mat_camion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="conduce-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
         'listadoCamioneros' => $listadoCamioneros,
         'listadoCamiones' => $listadoCamiones,
    ]) ?>

</div>
