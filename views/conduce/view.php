<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Conduce */

$this->title = $model->dni_camionero;
$this->params['breadcrumbs'][] = ['label' => 'Conduces', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="conduce-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'dni_camionero' => $model->dni_camionero, 'mat_camion' => $model->mat_camion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'dni_camionero' => $model->dni_camionero, 'mat_camion' => $model->mat_camion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dni_camionero',
            'mat_camion',
        ],
    ]) ?>

</div>
