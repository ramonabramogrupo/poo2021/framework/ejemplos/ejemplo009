<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conduce */

$this->title = 'Create Conduce';
$this->params['breadcrumbs'][] = ['label' => 'Conduces', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conduce-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listadoCamioneros' => $listadoCamioneros,
        'listadoCamiones' => $listadoCamiones
    ]) ?>

</div>
