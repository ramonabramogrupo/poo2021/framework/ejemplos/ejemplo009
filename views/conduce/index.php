<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conduces';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conduce-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Conduce', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dni_camionero',
            'mat_camion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
