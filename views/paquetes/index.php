<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Paquetes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paquetes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Paquetes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo',
            'descripcion',
            'direccion',
            'destinatario',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete} {update} ',
                'buttons' => [
                    'view' => function ($url,$model){
                        return Html::a('<i class="fas fa-eye"></i>',['view','codigo'=>$model->codigo]);
                    },
                    'update' => function ($url,$model){
                        return Html::a('<i class="fas fa-edit"></i>',['update','codigo'=>$model->codigo]);
                    },
                    'delete' => function ($url,$model){
                        return Html::a('<i class="fas fa-trash-alt"></i>', ['delete', 'codigo' => $model->codigo], [
                            'data' => [
                                'confirm' => '¿Estas seguro de querer eliminar el registro?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                    ]
                
                ],
        ],
    ]); ?>


</div>
