<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuye */

$this->title = $model->dni_camionero;
$this->params['breadcrumbs'][] = ['label' => 'Distribuyes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="distribuye-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'dni_camionero' => $model->dni_camionero, 'cod_paquete' => $model->cod_paquete], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'dni_camionero' => $model->dni_camionero, 'cod_paquete' => $model->cod_paquete], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dni_camionero',
            'cod_paquete',
        ],
    ]) ?>

</div>
