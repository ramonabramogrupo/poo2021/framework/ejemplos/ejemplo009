<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Distribuyes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribuye-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Distribuye', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dni_camionero',
            'cod_paquete',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
