<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuye */

$this->title = 'Create Distribuye';
$this->params['breadcrumbs'][] = ['label' => 'Distribuyes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribuye-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
