<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuye */

$this->title = 'Update Distribuye: ' . $model->dni_camionero;
$this->params['breadcrumbs'][] = ['label' => 'Distribuyes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dni_camionero, 'url' => ['view', 'dni_camionero' => $model->dni_camionero, 'cod_paquete' => $model->cod_paquete]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="distribuye-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
