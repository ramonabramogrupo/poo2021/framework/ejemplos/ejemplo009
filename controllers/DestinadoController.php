<?php

namespace app\controllers;

use app\models\Destinado;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DestinadoController implements the CRUD actions for Destinado model.
 */
class DestinadoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Destinado models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Destinado::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'cod_provincia' => SORT_DESC,
                    'cod_paquete' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Destinado model.
     * @param int $cod_provincia Cod Provincia
     * @param int $cod_paquete Cod Paquete
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_provincia, $cod_paquete)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_provincia, $cod_paquete),
        ]);
    }

    /**
     * Creates a new Destinado model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Destinado();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cod_provincia' => $model->cod_provincia, 'cod_paquete' => $model->cod_paquete]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Destinado model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cod_provincia Cod Provincia
     * @param int $cod_paquete Cod Paquete
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_provincia, $cod_paquete)
    {
        $model = $this->findModel($cod_provincia, $cod_paquete);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_provincia' => $model->cod_provincia, 'cod_paquete' => $model->cod_paquete]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Destinado model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cod_provincia Cod Provincia
     * @param int $cod_paquete Cod Paquete
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_provincia, $cod_paquete)
    {
        $this->findModel($cod_provincia, $cod_paquete)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Destinado model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cod_provincia Cod Provincia
     * @param int $cod_paquete Cod Paquete
     * @return Destinado the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_provincia, $cod_paquete)
    {
        if (($model = Destinado::findOne(['cod_provincia' => $cod_provincia, 'cod_paquete' => $cod_paquete])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
