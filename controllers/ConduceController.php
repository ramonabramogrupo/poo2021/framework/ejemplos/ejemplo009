<?php

namespace app\controllers;

use app\models\Conduce;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Camioneros;
use app\models\Camion;
use yii\helpers\ArrayHelper;

/**
 * ConduceController implements the CRUD actions for Conduce model.
 */
class ConduceController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Conduce models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Conduce::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'dni_camionero' => SORT_DESC,
                    'mat_camion' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Conduce model.
     * @param string $dni_camionero Dni Camionero
     * @param string $mat_camion Mat Camion
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dni_camionero, $mat_camion)
    {
        return $this->render('view', [
            'model' => $this->findModel($dni_camionero, $mat_camion),
        ]);
    }

    /**
     * Creates a new Conduce model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Conduce();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'dni_camionero' => $model->dni_camionero, 'mat_camion' => $model->mat_camion]);
            }
        } else {
            $model->loadDefaultValues();
        }
        
        // creo una variable de tipo array con todos los camioneros
        // select * from camioneros
        $camioneros= Camioneros::find()->all();
        
        // creo una variable de tipo array con todos los camiones
        // select * from camion;
        $camiones= Camion::find()->all();
        
        // crear una array donde el indice sea el dni del camionero y el valor el nombre
        $listadoCamioneros= ArrayHelper::map($camioneros,'dni','nombre');
        
        // crear un array donde el indice sea la matricula del camion y el valor el modelo
        $listadoCamiones= ArrayHelper::map($camiones,'matricula','modelo');
                
        
        return $this->render('create', [
            'model' => $model,
            'listadoCamioneros' => $listadoCamioneros,
            'listadoCamiones' => $listadoCamiones,
        ]);
    }

    /**
     * Updates an existing Conduce model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $dni_camionero Dni Camionero
     * @param string $mat_camion Mat Camion
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dni_camionero, $mat_camion)
    {
        $model = $this->findModel($dni_camionero, $mat_camion);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'dni_camionero' => $model->dni_camionero, 'mat_camion' => $model->mat_camion]);
        }

         // creo una variable de tipo array con todos los camioneros
        // select * from camioneros
        $camioneros= Camioneros::find()->all();
        
        // creo una variable de tipo array con todos los camiones
        // select * from camion;
        $camiones= Camion::find()->all();
        
        // crear una array donde el indice sea el dni del camionero y el valor el nombre
        $listadoCamioneros= ArrayHelper::map($camioneros,'dni','nombre');
        
        // crear un array donde el indice sea la matricula del camion y el valor el modelo
        $listadoCamiones= ArrayHelper::map($camiones,'matricula','modelo');
        
        
        return $this->render('update', [
            'model' => $model,
            'listadoCamioneros' => $listadoCamioneros,
            'listadoCamiones' => $listadoCamiones,
        ]);
    }

    /**
     * Deletes an existing Conduce model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $dni_camionero Dni Camionero
     * @param string $mat_camion Mat Camion
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dni_camionero, $mat_camion)
    {
        $this->findModel($dni_camionero, $mat_camion)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Conduce model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $dni_camionero Dni Camionero
     * @param string $mat_camion Mat Camion
     * @return Conduce the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dni_camionero, $mat_camion)
    {
        if (($model = Conduce::findOne(['dni_camionero' => $dni_camionero, 'mat_camion' => $mat_camion])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
