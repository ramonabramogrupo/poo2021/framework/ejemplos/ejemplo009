<?php

namespace app\controllers;

use app\models\Distribuye;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DistribuyeController implements the CRUD actions for Distribuye model.
 */
class DistribuyeController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Distribuye models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Distribuye::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'dni_camionero' => SORT_DESC,
                    'cod_paquete' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Distribuye model.
     * @param string $dni_camionero Dni Camionero
     * @param int $cod_paquete Cod Paquete
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dni_camionero, $cod_paquete)
    {
        return $this->render('view', [
            'model' => $this->findModel($dni_camionero, $cod_paquete),
        ]);
    }

    /**
     * Creates a new Distribuye model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Distribuye();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'dni_camionero' => $model->dni_camionero, 'cod_paquete' => $model->cod_paquete]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Distribuye model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $dni_camionero Dni Camionero
     * @param int $cod_paquete Cod Paquete
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dni_camionero, $cod_paquete)
    {
        $model = $this->findModel($dni_camionero, $cod_paquete);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'dni_camionero' => $model->dni_camionero, 'cod_paquete' => $model->cod_paquete]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Distribuye model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $dni_camionero Dni Camionero
     * @param int $cod_paquete Cod Paquete
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dni_camionero, $cod_paquete)
    {
        $this->findModel($dni_camionero, $cod_paquete)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Distribuye model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $dni_camionero Dni Camionero
     * @param int $cod_paquete Cod Paquete
     * @return Distribuye the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dni_camionero, $cod_paquete)
    {
        if (($model = Distribuye::findOne(['dni_camionero' => $dni_camionero, 'cod_paquete' => $cod_paquete])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
