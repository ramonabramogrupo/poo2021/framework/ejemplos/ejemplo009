<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camioneros".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $direccion
 * @property string|null $telefono
 * @property string|null $poblacion
 * @property float|null $salario
 *
 * @property Paquetes[] $codPaquetes
 * @property Conduce[] $conduces
 * @property Distribuye[] $distribuyes
 * @property Camion[] $matCamions
 */
class Camioneros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camioneros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['salario'], 'number'],
            [['dni'], 'string', 'max' => 12],
            [['nombre', 'direccion', 'poblacion'], 'string', 'max' => 100],
            [['telefono'], 'string', 'max' => 15],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'poblacion' => 'Poblacion',
            'salario' => 'Salario',
        ];
    }

    /**
     * Gets query for [[CodPaquetes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPaquetes()
    {
        return $this->hasMany(Paquetes::className(), ['codigo' => 'cod_paquete'])->viaTable('distribuye', ['dni_camionero' => 'dni']);
    }

    /**
     * Gets query for [[Conduces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConduces()
    {
        return $this->hasMany(Conduce::className(), ['dni_camionero' => 'dni']);
    }

    /**
     * Gets query for [[Distribuyes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuyes()
    {
        return $this->hasMany(Distribuye::className(), ['dni_camionero' => 'dni']);
    }

    /**
     * Gets query for [[MatCamions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatCamions()
    {
        return $this->hasMany(Camion::className(), ['matricula' => 'mat_camion'])->viaTable('conduce', ['dni_camionero' => 'dni']);
    }
}
