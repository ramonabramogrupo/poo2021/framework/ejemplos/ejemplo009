<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paquetes".
 *
 * @property int $codigo
 * @property string|null $descripcion
 * @property string|null $direccion
 * @property string|null $destinatario
 *
 * @property Provincias[] $codProvincias
 * @property Destinado $destinado
 * @property Distribuye $distribuye
 * @property Camioneros[] $dniCamioneros
 */
class Paquetes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquetes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'integer'],
            [['descripcion', 'direccion', 'destinatario'], 'string', 'max' => 100],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'descripcion' => 'Descripcion',
            'direccion' => 'Direccion',
            'destinatario' => 'Destinatario',
        ];
    }

    /**
     * Gets query for [[CodProvincias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodProvincias()
    {
        return $this->hasMany(Provincias::className(), ['codigo' => 'cod_provincia'])->viaTable('destinado', ['cod_paquete' => 'codigo']);
    }

    /**
     * Gets query for [[Destinado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDestinado()
    {
        return $this->hasOne(Destinado::className(), ['cod_paquete' => 'codigo']);
    }

    /**
     * Gets query for [[Distribuye]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuye()
    {
        return $this->hasOne(Distribuye::className(), ['cod_paquete' => 'codigo']);
    }

    /**
     * Gets query for [[DniCamioneros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniCamioneros()
    {
        return $this->hasMany(Camioneros::className(), ['dni' => 'dni_camionero'])->viaTable('distribuye', ['cod_paquete' => 'codigo']);
    }
}
