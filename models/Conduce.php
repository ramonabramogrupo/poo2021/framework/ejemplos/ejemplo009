<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conduce".
 *
 * @property string $dni_camionero
 * @property string $mat_camion
 *
 * @property Camioneros $dniCamionero
 * @property Camion $matCamion
 */
class Conduce extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conduce';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni_camionero', 'mat_camion'], 'required'],
            [['dni_camionero'], 'string', 'max' => 12],
            [['mat_camion'], 'string', 'max' => 7],
            [['dni_camionero', 'mat_camion'], 'unique', 'targetAttribute' => ['dni_camionero', 'mat_camion']],
            [['mat_camion'], 'exist', 'skipOnError' => true, 'targetClass' => Camion::className(), 'targetAttribute' => ['mat_camion' => 'matricula']],
            [['dni_camionero'], 'exist', 'skipOnError' => true, 'targetClass' => Camioneros::className(), 'targetAttribute' => ['dni_camionero' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni_camionero' => 'Dni Camionero',
            'mat_camion' => 'Mat Camion',
        ];
    }

    /**
     * Gets query for [[DniCamionero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniCamionero()
    {
        return $this->hasOne(Camioneros::className(), ['dni' => 'dni_camionero']);
    }

    /**
     * Gets query for [[MatCamion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatCamion()
    {
        return $this->hasOne(Camion::className(), ['matricula' => 'mat_camion']);
    }
}
