<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distribuye".
 *
 * @property string $dni_camionero
 * @property int $cod_paquete
 *
 * @property Paquetes $codPaquete
 * @property Camioneros $dniCamionero
 */
class Distribuye extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'distribuye';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni_camionero', 'cod_paquete'], 'required'],
            [['cod_paquete'], 'integer'],
            [['dni_camionero'], 'string', 'max' => 12],
            [['cod_paquete'], 'unique'],
            [['dni_camionero', 'cod_paquete'], 'unique', 'targetAttribute' => ['dni_camionero', 'cod_paquete']],
            [['dni_camionero'], 'exist', 'skipOnError' => true, 'targetClass' => Camioneros::className(), 'targetAttribute' => ['dni_camionero' => 'dni']],
            [['cod_paquete'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['cod_paquete' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni_camionero' => 'Dni Camionero',
            'cod_paquete' => 'Cod Paquete',
        ];
    }

    /**
     * Gets query for [[CodPaquete]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPaquete()
    {
        return $this->hasOne(Paquetes::className(), ['codigo' => 'cod_paquete']);
    }

    /**
     * Gets query for [[DniCamionero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniCamionero()
    {
        return $this->hasOne(Camioneros::className(), ['dni' => 'dni_camionero']);
    }
}
