<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camion".
 *
 * @property string $matricula
 * @property string|null $modelo
 * @property string|null $tipo
 * @property int|null $potencia
 *
 * @property Conduce[] $conduces
 * @property Camioneros[] $dniCamioneros
 */
class Camion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula'], 'required'],
            [['potencia'], 'integer'],
            [['matricula'], 'string', 'max' => 7],
            [['modelo', 'tipo'], 'string', 'max' => 100],
            [['matricula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'matricula' => 'Matricula',
            'modelo' => 'Modelo',
            'tipo' => 'Tipo',
            'potencia' => 'Potencia',
        ];
    }

    /**
     * Gets query for [[Conduces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConduces()
    {
        return $this->hasMany(Conduce::className(), ['mat_camion' => 'matricula']);
    }

    /**
     * Gets query for [[DniCamioneros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniCamioneros()
    {
        return $this->hasMany(Camioneros::className(), ['dni' => 'dni_camionero'])->viaTable('conduce', ['mat_camion' => 'matricula']);
    }
}
