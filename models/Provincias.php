<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provincias".
 *
 * @property int $codigo
 * @property string|null $nombre
 *
 * @property Paquetes[] $codPaquetes
 * @property Destinado[] $destinados
 */
class Provincias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[CodPaquetes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPaquetes()
    {
        return $this->hasMany(Paquetes::className(), ['codigo' => 'cod_paquete'])->viaTable('destinado', ['cod_provincia' => 'codigo']);
    }

    /**
     * Gets query for [[Destinados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDestinados()
    {
        return $this->hasMany(Destinado::className(), ['cod_provincia' => 'codigo']);
    }
}
