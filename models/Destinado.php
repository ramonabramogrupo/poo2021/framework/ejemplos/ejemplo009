<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "destinado".
 *
 * @property int $cod_provincia
 * @property int $cod_paquete
 *
 * @property Paquetes $codPaquete
 * @property Provincias $codProvincia
 */
class Destinado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'destinado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_provincia', 'cod_paquete'], 'required'],
            [['cod_provincia', 'cod_paquete'], 'integer'],
            [['cod_paquete'], 'unique'],
            [['cod_provincia', 'cod_paquete'], 'unique', 'targetAttribute' => ['cod_provincia', 'cod_paquete']],
            [['cod_paquete'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['cod_paquete' => 'codigo']],
            [['cod_provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['cod_provincia' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_provincia' => 'Cod Provincia',
            'cod_paquete' => 'Cod Paquete',
        ];
    }

    /**
     * Gets query for [[CodPaquete]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPaquete()
    {
        return $this->hasOne(Paquetes::className(), ['codigo' => 'cod_paquete']);
    }

    /**
     * Gets query for [[CodProvincia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodProvincia()
    {
        return $this->hasOne(Provincias::className(), ['codigo' => 'cod_provincia']);
    }
}
